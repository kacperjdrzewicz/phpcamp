﻿<?php
/* 
Zadania PHP

--- Zadanie 1 ---

echo '<a href="http://illli.li/kamp/index.phtml">link</a>';

echo 	'Uczestniczę w "PHP Camp".<br>
		Kiedyś będę zarabiał dużo $$$!<br>
		Wiem jak zapisać \'\' i "".<br>
		Wiem też jak zapisać \\\.<br>
		Moja ulubiona nazwa zmiennej to $iaiCamp.<br>
		Jest to tablica array(\'day1\', \'day2\'’, \'day3\').<br>
		Na pulpicie mam ukryty katalog D:\\nowe_filmy.';

--- Zadanie 2 ---

for($i = 0; $i < 100000; $i++) {
	$tablica[] = "Napis";
}

echo count($tablica);

$time_pre = microtime(true);
for($i = 0; $i < 100000; $i++) {
	if(strlen($tablica[$i]) > 5) {}
}
$time_post = microtime(true);

echo $time_post - $time_pre;

echo '<br>';

$time_pre = microtime(true);
for($i = 0; $i < 100000; $i++) {
	if(!empty($tablica[$i][5])) {}
}
$time_post = microtime(true);

echo $time_post - $time_pre;

--- Zadanie 3 ---

$roznica = strtotime("2017-12-25") - strtotime("2017-04-16");

echo $roznica/60/60/24;

--- Zadanie 4 ---

class Rezerwacja {
	public $nazwaPokoju;
	public $dataOd;
	public $dataDo;
	
	public function __construct($nazwaPokoju, $dataOd, $dataDo) {
		$this->nazwaPokoju = $nazwaPokoju;
		$this->dataOd = date("d.m.y", $dataOd);
		$this->dataDo = date("d.m.y", $dataDo);
    }
	
	public function wyswietl() {
		echo "Pokój $this->nazwaPokoju wynajety od $this->dataOd do $this->dataDo.";
	}
	
	public function __toString() {
	}
}

$zmienna = new Rezerwacja("Hej", time(), strtotime("2017-12-25"));

$zmienna->wyswietl();

--- Zadanie 5 ---

$homepage = file_get_contents('http://www.home.pl/');
echo $homepage;

--- Zadanie 6 ---

$xml = simplexml_load_file('reservation.xml');
$json = json_encode($xml);
$array = json_decode($json,TRUE);

print "<pre>";
print_r($array);
print "</pre>";
*/

/* 
Zadania SQL

--- Zadanie 1 ---

SELECT count(*) FROM `clients` WHERE date_of_birth > '1999-12-31' AND gender = 'male'
SELECT gender, count(*) FROM `clients` GROUP BY gender
SELECT id FROM `clients` WHERE orders_count = (SELECT MAX(orders_count) FROM `clients`)
SELECT count(*) FROM `clients` WHERE name LIKE 'T%' OR surname LIKE 'T%'
SELECT count(*) FROM (SELECT DISTINCT city FROM `clients`) as tab
*/

interface iDB {
    /**
     * Wykonuje zapytanie, zapisuje uchwyt
     * @param string $query Zapytanie do wykonania
     * @return bool Czy się udało wykonać zapytanie czy nie
     */
    public function query($query);

    /**
     * Zwraca liczbę wierszy zmodyfikowanych przez ostatnie zapytanie
     * @return int
     */
    public function getAffectedRows();

    /**
     * Zwraca jeden (kolejny) wiersz z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getRow();

    /**
     * Zwraca wszystkie wiersze z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getAllRows();
}

class DB implements iDB {
	public $mysqli;
	public $query;
	
	public function __construct($host, $login, $password, $dbName) {
		$this->mysqli = mysqli_connect($host, $login, $password, $dbName);
	}

    public function query($query) {
		$this->query = mysqli_query($this->mysqli, $query);
	}

    public function getAffectedRows() {
		return mysqli_affected_rows($this->mysqli);
	}

    public function getRow() {
		return mysqli_fetch_row($this->query);
	}

    public function getAllRows() {
		$results = array();
        while (($row = $this->getRow())) {
            $results[] = $row;
        }
        return $results;
	}
}

$baza = new DB('127.0.0.1', 'root', '', 'phpcamp_kdrzewicz');
?>